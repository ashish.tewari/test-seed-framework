package com.axistest.testSeed

import com.axis.seed.CsvImporter

fun main() {
    CsvImporter("cities").import()
}